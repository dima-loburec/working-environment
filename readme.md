- [Mac](mac/)
- [Windows](windows/)
- [Ubuntu](ubuntu/)
- [List of short cuts](short_cuts.md)
- [Sublime Text](sublime/)
- [PhpStorm](phpstorm/)
- [Vagrant](vagrant/)
- [Terminal](terminal/)

List of things to be installed for work:

[https://gist.github.com/loburets/954c1b013a56ada504798e6dbca2a2fd](https://gist.github.com/loburets/954c1b013a56ada504798e6dbca2a2fd)
